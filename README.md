# my-alarm-sample
[index 화면]
1. index 화면에서 버튼 클릭 시 main 화면으로 넘어가도록 한다.(SPA, vue-router)
[main 화면]
1. 알람을 등록할 수 있도록 화면을 갖추고, 알람을 저장할 수 있도록 한다.
백엔드 서버가 없으므로 저장 기능은 없다( 또는 파일/localStorage 저장? )
2. 최상단에 현재 시간을 표시한다.
3. 등록한 알람과 현재 시간을 비교하여, 시간이 되면 10초간? 알람 정보와 함께 알람이 표시되도록 한다.

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
