import Vue from 'vue'
import Router from 'vue-router'
import NProgress from 'nprogress'
import '../node_modules/nprogress/nprogress.css'
import Home from './views/Home'
import Main from './views/Main'

Vue.use(Router)

const router = new Router({
	mode: 'history',
	base: process.env.BASE_URL,
	routes: [
		{
			path: '/',
			name: 'home',
			component: Home
		},
		{
			path: '/main',
			name: 'main',
			component: Main

		}
	]
})

router.beforeResolve((to, from, next) => {
	// If this isn't an initial page load.
	// Start the route progress bar.
	NProgress.start()
	next()
})

router.afterEach(() => {
	// Complete the animation of the route progress bar.
	NProgress.done()
})

export default router
